/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package pizzario.utils;

import java.util.UUID;

public abstract class DomainEntity {

    protected DomainEntity ( UUID domainId ) {

        if ( domainId == null )
            throw new IllegalArgumentException( "domainId" );

        this.domainId = domainId;
    }

    public UUID getDomainId () {
        return this.domainId;
    }

    @Override
    public boolean equals ( Object o ) {
        if ( o.getClass() != this.getClass() )
            return false;

        DomainEntity otherDomainEntity = ( DomainEntity ) o;
        return domainId.equals( otherDomainEntity.getDomainId() );
    }

    @Override
    public int hashCode () {
        return domainId.hashCode();
    }

    private UUID domainId;
}
