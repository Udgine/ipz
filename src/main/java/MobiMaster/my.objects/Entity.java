package my.objects;

import java.util.UUID;

public abstract class Entity {
    private UUID uuid;

    public UUID getUuid(){
        return uuid;
    }

    public void setUuid(UUID uuid){
        this.uuid = uuid;
    }

    protected Entity(UUID uuid){
        if (uuid == null)
            throw new IllegalArgumentException("id");

        this.uuid = uuid;
    }

    @Override
    public boolean equals(Object object){
        if (this == object)
            return true;

        if (object == null || this.getClass() != object.getClass())
            return false;

        Entity entity = (Entity)object;
        return uuid == entity.uuid;
    }

    @Override
    public int hashCode() {
        return uuid.hashCode();
    }
}
