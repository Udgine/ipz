package my.objects;

import java.util.UUID;

public class Balance extends Entity{
    private double balanceValue;
    public static final int MAXBALANCEVALUE = 2000;
    public static final int MINBALANCEVALUE = 0;

    public double getBalanceValue() {
        return balanceValue;
    }

    public void setBalanceValue(double balanceValue) {
        this.balanceValue = balanceValue;
    }

    public Balance(UUID uuid, double balanceValue) {
        super(uuid);
        if (balanceValue < MINBALANCEVALUE || balanceValue > MAXBALANCEVALUE)
            throw new IllegalArgumentException("balance");

        this.balanceValue = balanceValue;
    }

    @Override
    public String toString(){
        return String.format("===%s===\n", balanceValue);
    }
}
