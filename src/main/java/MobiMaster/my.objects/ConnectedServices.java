package my.objects;

import java.util.ArrayList;
import java.util.List;

public class ConnectedServices {
    private List<Service> serviceList;

    public ConnectedServices(){
        serviceList = new ArrayList<Service>();
    }

    public void connectService(Service service){

        if (service == null)
            throw new IllegalArgumentException("service");

        serviceList.add(service);
    }

    @Override
    public String toString(){
        return String.format("===%s===\n", serviceList);
    }
}
