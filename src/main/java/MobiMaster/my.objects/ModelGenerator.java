package my.objects;

import java.util.UUID;

public class ModelGenerator {

    private static Service fService, sService, tService;
    private static Tariff fTariff, sTariff, tTariff;
    private static Account account;
    private static OperatorAccount operatorAccount;
    private static Model model;
    public static Model generateData(){
        model = new Model();

        generateServices();
        generateTariffs();
        generateAccounts();
        generateOperatorAccounts();
        generateChangeTariff();
        generateConnectService();
        generateAddBalance();

        return model;
    }

    private static void generateServices(){
        fService = new Service(UUID.randomUUID(), "fService");
        sService = new Service(UUID.randomUUID(), "sService");
        tService = new Service(UUID.randomUUID(), "tService");
        model.getServiceList().add(fService);
        model.getServiceList().add(sService);
        model.getServiceList().add(tService);
    }

    private static void generateTariffs(){
        fTariff = new Tariff(UUID.randomUUID(), "fTariff");
        sTariff = new Tariff(UUID.randomUUID(), "sTariff");
        tTariff = new Tariff(UUID.randomUUID(), "tTariff");
        model.getTariffList().add(fTariff);
        model.getTariffList().add(sTariff);
        model.getTariffList().add(tTariff);
    }

    private static void generateAccounts(){
        Contact contact = new Contact("Home", "Phone");
        Balance balance = new Balance(UUID.randomUUID(), 2000);
        ConnectedServices connectedServices = new ConnectedServices();
        connectedServices.connectService(sService);
        InternetInfo internetInfo = new InternetInfo("Internet", "200MB");
        TariffState tariffState = new TariffState("Costs", "1.50", "0.20" , "0.50", "2.20");
        account = new Account(UUID.randomUUID(), "Name", "Email", "2", contact,
                "Image",sTariff, internetInfo, tariffState, connectedServices, "2", balance);
        model.getAccountList().add(account);
    }

    private static void generateOperatorAccounts(){
        Contact contact = new Contact("Home", "Phone");
        Balance balance = new Balance(UUID.randomUUID(), 2000);
        ConnectedServices connectedServices = new ConnectedServices();
        connectedServices.connectService(tService);
        InternetInfo internetInfo = new InternetInfo("Internet", "200MB");
        TariffState tariffState = new TariffState("Costs", "1.50", "0.20" , "0.50", "2.20");
        operatorAccount = new OperatorAccount(UUID.randomUUID(), "Name", "Email", "2", contact,
                "Image", tTariff, internetInfo, tariffState,connectedServices, "2", balance);
        model.getOperatorAccountList().add(operatorAccount);
    }

    private static void generateChangeTariff(){
        ChangeTariff changeTariff = new ChangeTariff(UUID.randomUUID(), sTariff, "Account");
        model.getChangeTariffList().add(changeTariff);
    }

    private static void generateConnectService(){
        ConnectService connectService = new ConnectService(UUID.randomUUID(), sService, "Account");
        model.getConnectServiceList().add(connectService);
    }

    private static void generateAddBalance(){
        Balance balance = new Balance(UUID.randomUUID(), 2000);
        AddBalance addBalance = new AddBalance(UUID.randomUUID(), balance, "Account");
        model.getAddBalanceList().add(addBalance);
    }

    public static void main(String[] args){
        Model model = ModelGenerator.generateData();
        ModelState modelState = new ModelState(System.out);
        modelState.modelView(model);
        System.out.println()
    }
}
