package my.objects;

public enum OperationStatus {
    WAITING, FINISHED
}
