package my.objects;

import java.io.PrintStream;
import java.util.List;

public class ModelState {
    private PrintStream printStream;

    public ModelState(PrintStream printStream){
        this.printStream = printStream;
    }

    public void modelView(Model model){
        modelPrint("Services", model.getServiceList());
        modelPrint("Tariffs",model.getTariffList());
        modelPrint("Accounts",model.getAccountList());
        modelPrint("OperatorAccounts",model.getOperatorAccountList());
        modelPrint("ChangeTarrifs",model.getChangeTariffList());
        modelPrint("ConnectServices",model.getConnectServiceList());
        modelPrint("AddBalances",model.getAddBalanceList());
    }

    public void modelPrint(String name, List list){
        printStream.format("===%s===\n", name);

        for (Object object : list) {
            printStream.print(object);
        }
    }
}
