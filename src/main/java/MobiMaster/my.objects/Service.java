package my.objects;

import java.util.UUID;

public class Service extends Entity{
    private String name;

    public Service(UUID uuid, String name){
        super(uuid);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString(){
        return String.format("===%s===\n", getUuid() + " " + name);
    }
}
