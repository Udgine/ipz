package my.objects;

public class InternetInfo {
    private String name;
    private String internet;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInternet() {
        return internet;
    }

    public void setInternet(String internet) {
        this.internet = internet;
    }

    public InternetInfo(String name,String internet){
        this.name = name;
        this.internet = internet;
    }

    @Override
    public String toString(){
        return String.format("===%s===\n", name + " " + internet);
    }
}
