package my.objects;

public class Contact {
    private String address;
    private String phone;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Contact(String address, String phone){
        this.address = address;
        this.phone = phone;
    }

    @Override
    public String toString(){
        return String.format("===%s===\n", address + " " + phone);
    }
}
