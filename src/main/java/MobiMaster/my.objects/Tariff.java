package my.objects;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Tariff extends Entity{
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Tariff(UUID uuid, String name) {
        super(uuid);
        this.name = name;
    }

    @Override
    public String toString(){
        return String.format("===%s===\n", getUuid() + " " + name);
    }
}
